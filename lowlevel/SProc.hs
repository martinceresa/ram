module SProc where

import Control.Monad.State
import Data.Map.Strict as Map
import Data.List
import qualified Data.Vector as V
import TRam
import VRam

genop :: (Integer -> Integer -> Integer)
        -> Reg -> Reg -> Reg -> STERam ()
genop f rd r1 r2 = do
    vr1 <- getReg r1
    vr2 <- getReg r2
    setReg rd (f vr1 vr2)

processAI :: AInstr -> STERam ()
processAI (Const r i) = do
    setReg r i
processAI (Add rr r1 r2) = genop (+) rr r1 r2
processAI (Minus rr r1 r2) = genop (-) rr r1 r2

setBoolReg :: Bool -> Reg -> STERam ()
setBoolReg True r = setReg r 1
setBoolReg False r = setReg r 0

processBI :: BInstr -> STERam ()
processBI (And rd r2 r3) =
    genop (\ v1 v2 -> if (v1 > 0 && v2 > 0) then 1 else 0) rd r2 r3
processBI (Or rd r2 r3) = 
    genop (\ v1 v2 -> if (v1 > 0 || v2 > 2) then 1 else 0) rd r2 r3
processBI (Eq rd r2 r3) = 
    genop (\ v1 v2 -> if (v1 == v2) then 1 else 0) rd r2 r3
processBI (Less rd r2 r3) = do
    genop (\ v1 v2 -> if (v1 < v2) then 1 else 0) rd r2 r3

processIA :: IAccessInstr -> STERam ()
processIA (MScalar rd rs) = do
    v <- getReg rs
    v' <- getReg v
    setReg rd v'

processCI :: CInstr -> STERam ()
processCI (CJump r ni) = do
    v <- getReg r
    if (v > 0) then
        setPC ni
    else
        stepPC

process :: Instr -> STERam ()
process (AI instr) = do
    processAI instr
    stepPC
process (BI instr) = do
    processBI instr
    stepPC
process (IA instr) = do
    processIA instr
    stepPC
process (CI i) = processCI i
