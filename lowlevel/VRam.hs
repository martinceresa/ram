module VRam where

import Prelude hiding ((!!))
import TRam 
import Control.Monad.State
import Control.Monad.Except
import Data.Map.Strict as M
import qualified Data.Vector as V

stepPC' :: STERam ()
stepPC' = do
    (RAM sm vm pc) <- get
    put (RAM sm vm (pc+1))

raiseDiffSz :: [Vec] -> STERam ()
raiseDiffSz lv = do
    now <- getPC
    lift $ throwError $ DiffSz now lv

raiseIdxErr :: STERam ()
raiseIdxErr = do
    now <- getPC
    lift $ throwError $ IndexErr now

stepPC :: STERam ()
stepPC = do
    (RAM sm vm pc) <- get
    put (RAM sm vm (pc+1))

getPC :: STERam Int
getPC = do
    (RAM _ _ pc) <- get
    return pc

setPC :: Int -> STERam ()
setPC c = do
    (RAM sm vm _) <- get
    put (RAM sm vm c)

setReg :: Reg -> Integer -> STERam ()
setReg r i = do
    (RAM sm vm pc) <- get
    let nsm = M.insert r i sm
    put (RAM nsm vm pc)

(!!) = (M.!)

getReg :: Reg -> STERam Integer
getReg r = do
    (RAM sm _ _) <- get
    if (member r sm) then
        return (sm!!r)
    else
        do
            setError ('R' : show r ++ " doesn't have any value")
            return 0

setVec :: Vec -> VectI -> STERam ()
setVec v vi = do
    (RAM sm vm pc) <- get
    let nvm = insert v vi vm
    put (RAM sm nvm pc)

(!.!) = (M.!)

getVec :: Vec -> STERam VectI
getVec v = do
    (RAM _ vm _) <- get
    if (member v vm) then
        return (vm!.!v)
    else
        do
            setError ('V' : show v ++ " doesn't have any value")
            return (V.empty)

setError :: String -> STERam ()
setError s = do
    now <- getPC
    lift $ throwError $ BadForm now s
    setPC (-1) -- Finish computation
