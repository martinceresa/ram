{-# LANGUAGE GADTs, TypeFamilies #-}

module AbsMod where

-- This is what I think it is a RAM
-- The most abstract way

class RAM a where
    type SI :: *
    type SB :: *
    
    lSi     :: SI -> a SI
    lSb     :: SB -> a SB
    (+)     :: a SI -> a SI -> a SI
    (-)     :: a SI -> a SI -> a SI
    (&&)    :: a SB -> a SB -> a SB
    (||)    :: a SB -> a SB -> a SB
    (==)    :: a SI -> a SI -> a SB
    (<)     :: a SI -> a SI -> a SB

class RAM a => VRAM a where
    type Vector :: * -> *

    lVi     :: [SI] -> a (Vector SI)
    lVb     :: [SB] -> a (Vector SB)
    
    (|+)     :: a (Vector SI) -> a (Vector SI) -> a (Vector SI)
    (|-)     :: a (Vector SI) -> a (Vector SI) -> a (Vector SI)
    (|&&)    :: a (Vector SB) -> a (Vector SB) -> a (Vector SB)
    (|||)    :: a (Vector SB) -> a (Vector SB) -> a (Vector SB)
    (|==)    :: a (Vector SI) -> a (Vector SI) -> a (Vector SB)
    (|<)     :: a (Vector SI) -> a (Vector SI) -> a (Vector SB)

class VRAM a => SCANRAM a where
    scan :: (SI -> SI -> SI) -> a (Vector SI) -> a (Vector SI)
