{-# LANGUAGE QuasiQuotes #-}

import TRam
import QRam
import QRam.Quote
import MRam
import Data.Vector

simpleadd :: [VRamInstr]
simpleadd = [rame|  R0 <- 5
     R1 <- 2
     R0 <- R0 + R1|]

indirect :: [VRamInstr]
indirect = [rame|
    R0 <- 23
    R23 <- 4
    R1 <- (R0)
    R0 <- R1 + R23
    R10 <- R23 - R1
    R11 <- R23 ^ R10
    R12 <- R23 | R1 |]

shortBucl :: [VRamInstr]
shortBucl = [rame|
    R0 <- 10
    R2 <- 0
    R1 <- 1
    R0 <- R0 - R1
    R2 <- R2 + R1
    JUMP R0 3 |]

vectsuma = [rame|
    V1 <- [1 2 3]|]
