module VProc where

import Prelude hiding ((!!))
import Control.Monad.State
import Data.Map.Strict as Map
import Data.List
import qualified Data.Vector as V
import TRam
import VRam

-- Return the two vectors...
chkSameSz :: Vec -> Vec -> STERam (V.Vector Integer, V.Vector Integer)
chkSameSz v1 v2 = do
    sv1 <- getVec v1
    sv2 <- getVec v2
    if (V.length sv1 == V.length sv2) then
        do
            raiseDiffSz [v1,v2]
            return (sv1,sv2) -- Esto es sucio...
    else
        return (sv1,sv2)

genproc ::
        (Integer -> Integer -> Integer)
    ->  Vec -> Vec -> Vec
    ->  STERam ()
genproc op v1 v2 vd =  do
    vv1 <- getVec v1
    vv2 <- getVec v2
    if (V.length vv1 == V.length vv2) then
        do
            let nv = V.zipWith op vv1 vv2
            setVec vd nv
    else
        raiseDiffSz [v1,v2]

processVAI :: VAIntr -> STERam ()
processVAI (VAdd vd v1 v2) = genproc (+) v1 v2 vd
processVAI (VMinus vd v1 v2) = genproc (-) v1 v2 vd
processVAI (VAnd vd v1 v2) = genproc (\ l r -> if (l > 0) &&  (r > 0) then 1 else 0) v1 v2 vd
processVAI (VOr vd v1 v2) = genproc (\ l r -> if (l > 0) ||  (r > 0) then 1 else 0) v1 v2 vd
processVAI (VEq vd v1 v2) = genproc (\ l r -> if (l == r) then 1 else 0) v1 v2 vd
processVAI (VLess vd v1 v2) = genproc (\ l r -> if (l < r) then 1 else 0) v1 v2 vd
processVAI (VConst vd v1) = setVec vd v1
processVAI (PSel vd v1 v2 v3) = do
    sv1 <- getVec v1
    sv2 <- getVec v2
    sv3 <- getVec v3
    if (V.length sv1 == V.length sv2 && V.length sv2 == V.length sv3) then
        do
            let nvd = V.zipWith3 (\ e1 e2 e3 -> if (e3 > 0) then e1 else e2) sv1 sv2 sv3
            setVec vd nvd
    else
        raiseDiffSz [v1,v2,v3]

processPE :: PInstr -> STERam ()
processPE (PSelPermute vdest vdata vindex vsel vdef) = do
    vd <- getVec vdata
    vi <- getVec vindex
    vs <- getVec vsel
    vde <- getVec vdef
    if (V.length vd == V.length vs && V.length vs == V.length vi) then
        -- let tv = V.filter (\ (b,_,_) -> b) (V.zipWith3 (\ b i d -> (b,fromInteger i, d)) vs vi vd)
        let tv = V.foldr (\ (b,i,d) vs -> if b > 0 then V.cons (i,d) vs else vs) (V.empty) (V.zipWith3 (\ b i d -> (b,fromInteger i, d)) vs vi vd)
        in  if (V.length tv <= V.length vde) then
                do
                    let nv = V.update vde tv
                    setVec vdest nv
            else
                setError "Difference between default and the resulting vector..." 
    else
        raiseDiffSz [vdata, vindex, vsel]
processPE (PPermute vdest vdata vindex) = do
    vi <- getVec vindex
    vd <- getVec vdata
    if (V.length vi == V.length vd) then
        -- I don't really like this fromInteger... nor how I check that
        -- all the indexes are present.
        if (V.ifoldl (\ b i _ -> (V.elem (toInteger i) vi) && b ) True vd) then
            do
                let nv = V.update vd (V.zipWith (\ a b -> (fromInteger a, b)) vi vd)
                setVec vdest nv
        else
            setError ('V':(show vi) ++ "| All the new indices must be differents")
    else
        raiseDiffSz [vindex,vdata]

genscan :: (Integer -> Integer -> Integer) -> Vec -> Vec -> STERam ()
genscan f vd vs = do
    s <- getVec vs
    let s' = V.init s -- Drop the last element
    setVec vd (V.cons 0 $ V.scanl1 f s')

processSC :: ScanInstr -> STERam ()
processSC (AddScan vd vs) = genscan (+) vd vs
processSC (MaxScan vd vs) = genscan (max) vd vs
processSC (MinScan vd vs) = genscan (min) vd vs
processSC (OrScan vd vs) = genscan (\ a b -> if a > 0 || b > 0 then 1 else 0) vd vs
processSC (AndScan vd vs) = genscan (\ a b -> if a > 0 && b > 0 then 1 else 0) vd vs

vprocess :: VInstr -> STERam ()
vprocess (OW vi) = do
    processVAI vi
    stepPC
vprocess (PE pi) = do
    processPE pi
    stepPC
vprocess (SC si) = do
    processSC si
    stepPC

vsprocess :: VSInstr -> STERam ()
vsprocess (Insert vd vs i v) = do
    src <- getVec vs
    vi <- getReg i
    let vi' = fromInteger vi
    nv  <- getReg v
    if (vi' >= V.length src) then
        raiseIdxErr
    else
        let nvec = src V.// [(vi',nv)] in
        setVec vd nvec
vsprocess (Extract rd vs i) = do
    vsrc <- getVec vs
    vi <- getReg i
    let vi' = fromInteger vi
    case (vsrc V.!? vi') of
        Nothing -> raiseIdxErr
        Just x -> setReg rd x
vsprocess (Distribute vd rs i) = do
    v <- getReg rs
    vi <- getReg i
    let vi' = fromInteger vi
    setVec vd (V.replicate vi' v)
vsprocess (Length rd vs) = do
    vsrc <- getVec vs
    setReg rd (toInteger $ V.length vsrc)

vsproc :: VSInstr -> STERam ()
vsproc v = do
    vsprocess v
    stepPC
