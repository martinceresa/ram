module MRam where

import Control.Monad.State
import Control.Monad.Except
import Data.Map.Strict as Map
import Data.List
import qualified Data.Vector as V
import TRam
import VRam

import VProc
import SProc

loadProgram :: [VRamInstr] -> Program
loadProgram xs = Map.fromList (zip [0..] (xs ++ [FIN]))

r0 :: Reg
r0 = 0

processProg :: Program -> STERam Integer
processProg prog = do
    next <- getPC
    case next of
        (-1) -> do
                    r <- getReg r0
                    return $ r
        _ ->
            if (member next prog) then
                do
                    case (prog!next) of
                        SI i -> process i 
                        VI v -> vprocess v
                        VS vs -> vsproc vs
                        FIN -> setPC (-1)
                    processProg prog
            else
                do
                    setError "Jump outside the program"
                    processProg prog

compute :: Program -> Either ERam (Integer, Ram)
compute prog = runExcept $ runStateT (processProg prog) initSt

relativeJump :: Int -> VRamInstr -> VRamInstr
relativeJump i (SI (CI (CJump r s))) = SI $ CI $ CJump r (s+i)
relativeJump _ l = l

(<+>) :: Program -> Program -> Program
ml <+> mr = do
    let sml = Map.size ml
    let mlnofin = Map.delete sml ml
    let (ml',mll) = (Map.toList mlnofin,sml-1)
    let mr' = Data.List.map (\(p,i) -> (mll+p, relativeJump mll i)) (Map.toList mr)
    Map.fromList (ml' ++ mr')

run = compute . loadProgram
                 
suma :: [VRamInstr]
suma =
    [ SI $ AI $ Const 1 5
    , SI $ AI $ Const 2 10
    , SI $ AI $ Add r0 1 2
    ]

sumavect :: [VRamInstr]
sumavect =
    [ VI $ OW $ VConst 1 (V.fromList [1,1,1,1,1])
    , VI $ OW $ VConst 2 (V.fromList [2,2,2,2,2])
    , VI $ OW $ VMinus 3 1 2
    ]

ejsuma = compute $ loadProgram suma
ejvsuma = compute $ loadProgram sumavect

psel :: [VRamInstr]
psel =
    [ VI $ OW $ VConst 2 (V.fromList [5,1,3,4,3,9,2,6])
    , VI $ OW $ VConst 3 (V.fromList [2,5,3,8,1,3,6,2])
    , VI $ OW $ VConst 4 (V.fromList [1,0,0,0,1,1,0,1])
    , VI $ OW $ PSel 1 2 3 4
    ]

ejsel = compute $ loadProgram psel

joutside :: [VRamInstr]
joutside = 
    [ SI $ AI $ Const 0 4
    , SI $ CI $ CJump 0 25
    ]

joside = compute $ loadProgram joutside

bpermute :: [VRamInstr]
bpermute =
    [ VI $ OW $ VConst 2 (V.fromList [5,1,3,4,3,9,2,6])
    , VI $ OW $ VConst 3 (V.fromList [2,5,4,3,1,6,0,0])
    , VI $ PE $ PPermute 1 2 3
    ]

permute :: [VRamInstr]
permute =
    [ VI $ OW $ VConst 2 (V.fromList [5,1,3,4,3,9,2,6])
    , VI $ OW $ VConst 3 (V.fromList [2,5,4,3,1,6,0,7])
    , VI $ PE $ PPermute 1 2 3
    ]

jper = compute $ loadProgram permute

selpermute :: [VRamInstr]
selpermute =
    [ VI $ OW $ VConst 2 (V.fromList [5,1,3,4,3,9,2,6])
    , VI $ OW $ VConst 3 (V.fromList [2,5,4,6,1,3,0,7])
    , VI $ OW $ VConst 4 (V.fromList [1,0,0,0,0,1,0,0])
    , VI $ OW $ VConst 5 (V.fromList [1,2,3,4])
    , VI $ PE $ PSelPermute 1 2 3 4 5
    ]

jselper = compute $ loadProgram selpermute

sadd :: [VRamInstr]
sadd = 
    [ VI $ OW $ VConst 2 (V.fromList [5,1,3,4,3,9,2,6])
    , VI $ SC $ AddScan 1 2
    ]

jsadd = compute $ loadProgram sadd

smax :: [VRamInstr]
smax = 
    [ VI $ OW $ VConst 2 (V.fromList [5,1,3,4,3,9,2,6])
    , VI $ SC $ MaxScan 1 2
    ]

jsmax = compute $ loadProgram smax

-- Vector Scalar instructions
--
ex :: [VRamInstr]
ex =
    [ VI $ OW $ VConst 2 (V.fromList [5,1,3,4,3,9,2,6])
    , SI $ AI $ Const 1 9
    , SI $ AI $ Const 2 9
    , SI $ AI $ Const 3 5
    , VS $ Insert 3 2 2 1
    , VS $ Extract 2 2 2
    , VS $ Distribute 4 1 3
    , VS $ Length 3 2
    ]

jex = compute $ loadProgram ex
