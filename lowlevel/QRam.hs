module QRam where

import TRam

import Text.ParserCombinators.Parsec
import Data.Vector


-- emptyspace = skipMany (char ' ' <|> char '\n')
emptyspace = skipMany (char ' ' <|> char '\t')

lexeme p = do
    emptyspace
    x <- p
    emptyspace
    return x

integer :: CharParser st Integer
integer = lexeme $ do
    ds <- many1 digit
    return $ read ds

int :: CharParser st Int
int = lexeme $ do
    ds <- many1 digit
    return $ read ds

small = lower <|> char '_'
idvar = small <|> digit <|> char '\''

antiExprInt :: Reg -> CharParser st AInstr
antiExprInt r = lexeme $ do
    string "$int:"
    s <- many1 idvar
    return $ AntiConst r s

spacenum :: CharParser st [Integer]
spacenum = do
            emptyspace
            x <- integer
            xs <- spacenum
            return (x:xs)
    <|>
        return []

cvector :: CharParser st (Vector Integer)
cvector = do
    emptyspace
    xs <- between (lexeme $ char '[') (lexeme $ char ']') spacenum
    return (fromList xs)

clist :: CharParser st [Integer]
clist = do
    emptyspace
    xs <- between (lexeme $ char '[') (lexeme $ char ']') spacenum
    return xs
    
register = do
    char 'R'
    integer

vector = do
    char 'V'
    integer

fl :: CharParser st Char 
fl = do
    char '<'
    char '-'

soper :: Char -> CharParser st (Integer, Integer)
soper c = do
    r1 <- lexeme register
    lexeme (char c)
    r2 <- lexeme register
    return (r1,r2)
   
pop :: Char -> CharParser st Reg
pop c = do
    lexeme $ char c
    r2 <- lexeme register
    return r2

suma :: Reg -> Reg -> CharParser st (Either a AInstr)
suma r r1 = do
    r2 <- pop '+'
    return $ Right $ Add r r1 r2

minus :: Reg -> Reg -> CharParser st (Either a AInstr)
minus r r1 = do
    r2 <- pop '-'
    return $ Right $ Minus r r1 r2

eand :: Reg -> Reg -> CharParser st (Either BInstr b)
eand r r1 = do
    r2 <- pop '^'
    return $ Left $ And r r1 r2
 
eor :: Reg -> Reg -> CharParser st (Either BInstr b)
eor r r1 = do
    r2 <- pop '|'
    return $ Left $ Or r r1 r2
 
eq :: Reg -> Reg -> CharParser st (Either BInstr b)
eq  r r1= do
    r2 <- pop '='
    return $ Left $ Eq r r1 r2

less :: Reg -> Reg -> CharParser st (Either BInstr b)
less r r1 = do
    r2 <- pop '<'
    return $ Left $ Less r r1 r2

bop :: Reg -> CharParser st (Either BInstr AInstr)
bop r  =
    do 
        r1 <- register
        ((suma r r1)
         <|> (minus r r1)
         <|> (eand r r1)
         <|> (eor r r1)
         <|> (eq r r1)
         <|> (less r r1)
         )
     <|>
    do
        i <- integer
        return $ Right $ Const r i
     <|>
    do
        i <- antiExprInt r
        return $ Right $ i


cinstr :: CharParser st CInstr
cinstr = do
    lexeme $ string "JUMP"
    r <- lexeme register
    i <- lexeme int
    return (CJump r i)

ireg :: CharParser st Reg
ireg = do
    char '('
    rs <- register
    char ')'
    return rs

cexp :: CharParser st Instr
cexp =
    (do
       r <- lexeme register
       lexeme fl
       (do
            i <- bop r
            case i of
                Left b -> return $ BI b
                Right a -> return $ AI a
        <|>
        do
            rs <- lexeme ireg
            return $ IA $ MScalar r rs))
  <|>
    do
        c <- cinstr
        return $ CI c

-- Vector Scalar instruction that result in a Scalar
vssexp :: Reg ->  CharParser st VSInstr
vssexp r = do
    lexeme $ string "EXT"
    v <- lexeme vector
    i <- lexeme register
    return $ Extract r v i
    <|>
    do
        lexeme $ string "LEN"
        v <- lexeme vector
        return $ Length r v

scalarexp :: CharParser st VRamInstr
scalarexp = do
        c <- cinstr
        return $ SI $ CI c
        <|>
        do
            r <- lexeme $ register
            lexeme fl
            (do
                i <- bop r
                case i of
                    Left b -> return $ SI $ BI b
                    Right a -> return $ SI $ AI a
                <|>
                do
                    rs <- lexeme ireg
                    return $ SI $ IA $ MScalar r rs
                    <|>
                    do
                        vs <- vssexp r
                        return $ VS vs)


vexp' :: Vec -> CharParser st VAIntr
vexp' r =
         do
           v <- lexeme cvector
           return $ VConst r v
           <|>
             do
                v1 <- lexeme vector
                c <- lexeme $ anyChar 
                v2 <- lexeme vector
                return $ case c of
                    '+' -> VAdd r v1 v2
                    '-' -> VMinus r v1 v2
                    '^' -> VAnd r v1 v2
                    '|' -> VOr r v1 v2
                    '=' -> VEq r v1 v2
                    '<' -> VLess r v1 v2
                <|>
                 do
                    lexeme $ string "SEL"
                    v1 <- lexeme $ vector
                    v2 <- lexeme $ vector
                    v3 <- lexeme $ vector
                    return $ PSel r v1 v2 v3

scop :: String -> CharParser st Vec
scop st = do
    lexeme $ string st
    lexeme $ vector

scexp :: Vec -> CharParser st ScanInstr
scexp r = do
    s <- scop "+-SCAN"
    return $ AddScan r s
    <|>
    do
        char 'M'
        (do 
            s <- scop "AX-SCAN"
            return $ MaxScan r s
         <|>
         do
            s <- scop "IN-SCAN"
            return $ MinScan r s)
    <|>
    do
    s <- scop "OR-SCAN"
    return $ OrScan r s
    <|>
    do
    s <- scop "AND-SCAN"
    return $ AndScan r s

peexp :: Vec -> CharParser st PInstr
peexp r =
    do
        char 'P'
        (do
            lexeme $ string "ERMUTE"
            s1 <- lexeme $ vector
            s2 <- lexeme $ vector
            return $ PPermute r s1 s2
            <|>
            do
                lexeme $ string "SEL"
                s1 <- lexeme $ vector
                s2 <- lexeme $ vector
                s3 <- lexeme $ vector
                s4 <- lexeme $ vector
                return $ PSelPermute r s1 s2 s3 s4)

vexp :: Vec -> CharParser st VInstr
vexp r = do 
    v <- vexp' r
    return $ OW v
    <|>
    do
        sc <- scexp r
        return $ SC sc
    <|>
    do
        pe <- peexp r
        return $ PE pe

-- Vector Scalar instruction that result in Vector
vsvexp :: Vec -> CharParser st VSInstr
vsvexp r = do
    lexeme $ string "INS"
    v1 <- lexeme $ vector
    i <- lexeme $ register
    r <- lexeme $ register
    return $ Insert r v1 i r
    <|>
    do
    lexeme $ string "DIST"
    reg <- lexeme register
    i <- lexeme register
    return $ Distribute r reg i

resVec :: CharParser st VRamInstr
resVec = do
    r <- lexeme vector
    lexeme fl
    (do
        vi <- vexp r
        return $ VI vi
        <|>
        do
            vs <- vsvexp r
            return $ VS vs)
    

addconsI :: CharParser st VRamInstr
addconsI = do
        i <- lexeme cexp
        return (SI i)

statement :: CharParser st VRamInstr
statement = do
        emptyspace 
        (scalarexp <|> resVec)
            
programa :: CharParser st [VRamInstr]
programa = do
    x <- statement
    (do
        char '\n'
        xs <- programa
        return (x:xs)
      <|>
        return ([x]))

parsemagic :: Monad m => CharParser () a -> (String, Int, Int) -> String -> m a   
parsemagic par (file, line, col) s =
        case runParser p () "" s of
            Left err -> fail $ show err
            Right e -> return e
    where
        p = do
                pos <- getPosition
                setPosition $
                    (flip setSourceName) file $
                    (flip setSourceLine) line $
                    (flip setSourceColumn) col $
                    pos
                spaces
                e <- par
                eof
                return e
 
parsenone :: Monad m => String -> m [VRamInstr]
parsenone s = parsemagic programa ("",0,0) s    

parserExp :: Monad m => (String, Int, Int) -> String -> m [VRamInstr]
parserExp (file, line, col) s =
        case runParser p () "" s of
            Left err -> fail $ show err
            Right e -> return e
    where
        p = do
                pos <- getPosition
                setPosition $
                    (flip setSourceName) file $
                    (flip setSourceLine) line $
                    (flip setSourceColumn) col $
                    pos
                spaces
                e <- programa
                eof
                return e
            
