{-# LANGUAGE DeriveDataTypeable #-}
module TRam where

import Control.Monad.State
import Data.Map.Strict as Map
import Data.List
import Data.Typeable
import Data.Generics
import qualified Data.Vector as  V 
import Control.Monad.Except

type Reg = Integer

-- Arithmetic operations
-- Add rd r1 r2 = rd <- r1 + r2
data AInstr = Add Reg Reg Reg
            | Minus Reg Reg Reg
            | Const Reg Integer
            | AntiConst Reg String -- AntiQQ
    deriving (Show, Typeable, Data)

-- Logic operations
-- False : 0
-- True : t / t > 0
data BInstr = And Reg Reg Reg 
            | Or Reg Reg Reg
            | Eq Reg Reg Reg
            | Less Reg Reg Reg
    deriving (Show, Typeable, Data)

data CInstr = CJump Reg Int
    deriving (Show, Typeable, Data)

-- Indirect Access
data IAccessInstr = MScalar Reg Reg-- | MV.Vector
    deriving (Show, Typeable, Data)

data Instr = AI AInstr
            | BI BInstr
            | CI CInstr
            | IA IAccessInstr
    deriving (Show,Typeable,Data)

-- Let's start with the V.Vector Instrs
--
type Vec = Integer
type VectI = V.Vector Integer
-- Element wise operations
data VAIntr = VAdd Vec Vec Vec
            | VMinus Vec Vec Vec
            | VAnd Vec Vec Vec
            | VOr Vec Vec Vec
            | VEq Vec Vec Vec
            | VLess Vec Vec Vec
            | VConst Vec VectI
            | PSel Vec Vec Vec Vec
    deriving (Show,Typeable,Data)

data PInstr = PPermute Vec Vec Vec 
            | PSelPermute Vec Vec Vec Vec Vec
    deriving (Show,Typeable,Data)

data ScanInstr =  AddScan Vec Vec
                | MaxScan Vec Vec
                | MinScan Vec Vec
                | OrScan Vec Vec
                | AndScan Vec Vec
    deriving (Show,Typeable,Data)

data VInstr = OW VAIntr | PE PInstr | SC ScanInstr
    deriving (Show,Typeable,Data)

data VSInstr = Insert Vec Vec Reg Reg
            | Extract Reg Vec Reg
            | Distribute Vec Reg Reg
            | Length Reg Vec
    deriving (Show,Typeable,Data)

data VRamInstr = SI Instr | VI VInstr | VS VSInstr | FIN
    deriving (Show,Typeable,Data)

type Program = Map.Map Int VRamInstr
type ScalarMem = Map Reg Integer
type VectorMem = Map Vec VectI

data Ram = RAM {
     smem :: ScalarMem
    ,vmem :: VectorMem
    ,pc   :: Int
}

type Res = Either (Int,String) Integer
    
data ERam = DiffSz Int [Vec]
            | BadForm Int String
            | IndexErr Int
    deriving (Show,Typeable,Data)

-- If I also want the current RAM state:
-- type RERam = (ERam, RAM)
-- type STERam a = StateT Ram (Except RERam) a

type STERam a = StateT Ram (Except ERam) a

instance Show Ram where
    show (RAM sm vm pc) = 
        "\nCurrent Machine State!\n" ++
        "PC:" ++ show pc ++ "\n" ++ 
        "==============================\n" ++
        "Scalar Mem\n" ++
        "------------------------------\n" ++
        (Prelude.foldl (\xs (k,v) -> '\t' : ('R' : show k) ++ (':' : (show v)) ++ ('\n' : xs)) [] $ toList sm) ++
        "==============================\n" ++
        "Vector Mem\n" ++
        "------------------------------\n" ++
        (Prelude.foldl (\xs (k,v) -> '\t' : ('V' : show k) ++ (':' : (show v)) ++ ('\n' : xs)) [] $ toList vm) ++
        "==============================\n"

initSt :: Ram
initSt = RAM (Map.singleton 0 0) Map.empty 0

