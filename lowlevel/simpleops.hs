{-# LANGUAGE QuasiQuotes #-}

import TRam
import QRam
import QRam.Quote
import MRam
import Data.Vector hiding ((++))

-- Calling convention... Args come in order, and start from R1 or V1...
-- Results start from R0 and V0.
--
-- index *length*. takes a sacalar length and returns a vector of that length
-- with sequential indices
index :: Program -- [VRamInstr]
index = loadProgram $ [rame|
    R2 <- 1
    V1 <- DIST R2 R1
    V0 <- +-SCAN V1|]

abs_index :: Integer -> Vec -> Program
abs_index r2 vr = loadProgram $ 
    [ SI $ AI $ Const 2 1
    , SI $ AI $ Const 1 r2
    , VS $ Distribute 1 2 1
    , VI $ SC $ AddScan vr 1
    ]
-- We could just do that...

chkIndex = compute $ loadProgram [rame|R1 <- 8|] <+> index
chkAbsIndex = compute $ abs_index 8 0

-- AntiQQ version. Just playing around
index' :: Reg ->  Program --[VRamInstr]
index' r = loadProgram $ [rame|
    R2 <- $int:r
    V2 <- DIST R1 R2 
    V0 <- +-SCAN V1|]

-- (+)-reduce values. take a vector of values and combine all the elements of
-- the vector using one of five binary ops:
-- * + maximum minimum or and
--

aA :: Program --[VRamInstr]
aA = loadProgram [rame|
    V1 <- [5 1 3 4 3 9 2 6]|]

temp :: [VRamInstr]
temp = [rame|
    R1 <- LEN V1
    R4 <- 1
    R1 <- R1 - R4
    R2 <- EXT V1 R1
    R1 <- EXT V2 R1|]

-- max R1 R2 = R1>=R2?R0<-R1:R0<-R2
imax :: Program -- [VRamInstr]
imax = loadProgram $ [rame|
    R3 <- R2 < R1
    R4 <- 0
    R0 <- R1 + R4
    JUMP R3 5
    R0 <- R2 + R4|]

-- min R1 R2 = R1>=R2?R0<-R1:R0<-R2
imin :: Program -- [VRamInstr]
imin = loadProgram $ [rame|
    R3 <- R2 < R1
    R4 <- 0
    R0 <- R2 + R4
    JUMP R3 5
    R0 <- R1 + R4|]

add_reduce :: Program -- [VRamInstr]
add_reduce = do
    let sc = loadProgram $ ([rame| V2 <- +-SCAN V1|] ++ temp )
    let fadd = loadProgram $ [rame|R0 <- R1 + R2|]
    sc <+> fadd

max_reduce :: Program -- [VRamInstr]
max_reduce = do
    let sc = loadProgram $ [rame| V2 <- MAX-SCAN V1|] ++ temp
    sc <+> imax

min_reduce :: Program -- [VRamInstr]
min_reduce = do
    let sc = loadProgram $ [rame| V2 <- MIN-SCAN V1|] ++ temp
    sc <+> imin

or_reduce :: Program
or_reduce = loadProgram $ 
    [rame| V2 <- OR-SCAN V1|]
    ++
    temp
    ++
    [rame| R0 <- R1 | R2|]

aB :: Program
aB = loadProgram [rame|V1 <- [ 0 1 2 3 4 5 6]|] 
   
and_reduce :: Program
and_reduce = loadProgram $ 
    [rame| V2 <- AND-SCAN V1|]
    ++
    temp
    ++
    [rame| R0 <- R1 ^ R2|]

-- Distribute operations...
-- (+)-distribute values. Take a vector of values, combine all the eements of
-- the vector using one of five binary ops: +, maximum, minimum, or or and, and
-- distribute the values back across the vector.
--
distbody :: Program
distbody = loadProgram 
    [rame|
    R2 <- LEN V1
    V0 <- DIST R1 R2|]

politeDB :: Program
politeDB = loadProgram
    [rame| 
    R1 <- 0
    R1 <- R0 + R1|]
--
-- add_distribute (V1) => V0
add_distribute :: Program
add_distribute =
    add_reduce <+> politeDB <+> distbody

max_distribute :: Program
max_distribute =
    max_reduce <+>  politeDB <+>distbody

min_distribute :: Program
min_distribute =
    min_reduce <+> politeDB <+> distbody

or_distribute :: Program
or_distribute =
    or_reduce <+> politeDB <+> distbody

and_distribute :: Program
and_distribute =
    and_reduce <+> politeDB <+> distbody

-- append values1 values2. takes two vectors and appends them    
-- append V1 V2
append :: Program
append = loadProgram 
    [rame|
    R1 <- LEN V1
    R4 <- 0
    V8 <- DIST R4 R1
    V8 <- V1 + V8|]
    <+> index
    <+> 
    loadProgram [rame|
    R2 <- LEN V2
    R3 <- R1 + R2
    R4 <- 0
    R8 <- R1 + R4
    V3 <- DIST R4 R3
    R4 <- 1
    V4 <- DIST R4 R1
    V5 <- PSEL V8 V0 V4 V3
    R1 <- LEN V2|]
    <+> index
    <+> 
    loadProgram [rame|
    V6 <- DIST R8 R1
    V0 <- V0 + V6
    R4 <- 1
    V4 <- DIST R4 R1
    V0 <- PSEL V2 V0 V4 V5|]

af1 = loadProgram [rame|V1 <- [1 2 3]|]
af2 = loadProgram [rame|V2 <- [4 5]|]

chkappend = compute $ af1 <+> af2 <+> append
