{-# LANGUAGE GADTs, TypeFamilies #-}

module VRAM where

import qualified Data.Vector as V
import AbsMod -- This is the abstract ram... Talk with Mauro

type ScalarI = Integer
type ScalarB = Bool
type VectorI = V.Vector ScalarI
type VectorB = V.Vector ScalarB

data VRam a where
    -- Load Ops
    LScalarI :: ScalarI -> VRam ScalarI
    LScalarB :: ScalarB -> VRam ScalarB
    -- LVectorI :: [ScalarI] -> VRam VectorI
    -- LVectorB :: [ScalarB] -> VRam VectorB
    -- Scalar Instr | Arith and Logic
    SPlus :: VRam ScalarI -> VRam ScalarI -> VRam ScalarI
    SMinus :: VRam ScalarI -> VRam ScalarI -> VRam ScalarI
    SAnd :: VRam ScalarB -> VRam ScalarB -> VRam ScalarB
    SOr :: VRam ScalarB -> VRam ScalarB -> VRam ScalarB
    SLess :: VRam ScalarI -> VRam ScalarI -> VRam ScalarB
    SEq :: VRam ScalarI -> VRam ScalarI -> VRam ScalarB

instance RAM VRam where
    type SI = ScalarI
    type SB = ScalarB
    lSi = LScalarI
    lSb = LScalarB
    l + r = SPlus l r
    l - r = SMinus l r
    l && r = SAnd l r
    l || r = SOr l r
    l < r = SLess l r
    l == r = SEq l r

    -- -- Vector Instr | Element Wise
    -- VPlus :: VRam VectorI -> VRam VectorI -> VRam VectorI
    -- VMinus :: VRam VectorI -> VRam VectorI -> VRam VectorI
    -- VAnd :: VRam VectorB -> VRam VectorB -> VRam VectorB
    -- VOr :: VRam VectorB -> VRam VectorB -> VRam VectorB
    -- VLess :: VRam VectorI -> VRam VectorI -> VRam VectorB
    -- VPSEL :: VRam VectorB -> VRam VectorI -> VRam VectorI
    -- -- Vector Instr | Permute Instt
    -- VPerI :: VRam VectorI -> VRam VectorI -> VRam VectorI
    -- VPerB :: VRam VectorB -> VRam VectorI -> VRam VectorB 
