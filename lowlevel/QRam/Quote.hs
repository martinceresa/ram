{-# LANGUAGE QuasiQuotes #-}
module QRam.Quote (rame) where

import QRam
import TRam

import qualified Data.Text as T

import qualified Language.Haskell.TH as TH
import Language.Haskell.TH.Quote
import Data.Generics
import qualified Data.Vector as V

rame :: QuasiQuoter
rame = QuasiQuoter {
        quoteExp = quoteExprExp,
        quotePat = quoteExprPat,
        quoteType = undefined,
        quoteDec = undefined}

handleStr  :: String -> Maybe TH.ExpQ
handleStr x = Just $ TH.stringE x

inToExp :: [Integer] -> TH.Exp
inToExp is = TH.ListE $ map (TH.LitE . TH.IntegerL) is

handleVec :: V.Vector Integer -> Maybe TH.ExpQ
handleVec vs = 
    let is = V.toList vs in
    let qis = inToExp is in
    Just $ return $ TH.AppE (TH.VarE $ TH.mkName "Data.Vector.fromList") qis

handleAntiInt :: AInstr -> Maybe TH.ExpQ
handleAntiInt (AntiConst r s) = Just $ TH.appE
            (TH.appE
                    (TH.conE (TH.mkName "Const"))
                    (TH.litE $ TH.IntegerL r))
            (TH.varE $ TH.mkName s)
handleAntiInt _ = Nothing

quoteExprExp :: String -> TH.ExpQ
quoteExprExp s = do
        loc <- TH.location
        let pos = ( TH.loc_filename loc
                  , fst $ TH.loc_start loc
                  , snd $ TH.loc_start loc)
        rame <- parserExp pos s
        dataToExpQ (const Nothing
                    `extQ` handleVec
                    `extQ` handleAntiInt
                    ) rame

quoteExprPat :: String -> TH.PatQ
quoteExprPat s = do
        loc <- TH.location
        let pos = ( TH.loc_filename loc
                  , fst $ TH.loc_start loc
                  , snd $ TH.loc_start loc)
        rame <- parserExp pos s
        dataToPatQ (\ _ -> Nothing) rame
