# Simple implementation of a RAM model.

## RAM model

What ram model are we exaclty using? Ok, tough question...
I have followed the idea in [Cook & Reckhow][1]

>A *random access machine* (RAM) consists of a finite program operating on an
>infinite sequence of registers. Each register can hold an arbitrary integer.

I added some new operators, like the *logical module*...

## Scalar operations

Ok, there are a few things that we can do.

All the types, and commands are declared in TRam.hs.

Arithmetic operations. Three register mode, Rd <- R1 (+) R2

```
#!haskell
    data AInstr = Add Reg Reg Reg
                | Minus Reg Reg Reg
                | Const Reg Integer
```

Logic Operants. Same as arothmetic ops.

```
#!haskell
    data BInstr = And Reg Reg Reg 
                | Or Reg Reg Reg
                | Eq Reg Reg Reg
                | Less Reg Reg Reg
```

Conditional instrs. Just conditional jump, but could be more?

```
#!haskell
    data CInstr = CJump Reg Int
```

Indirect access intrs...

```
#!haskell
    data IAccessInstr = MScalar Reg Reg
```

And we can use QuasiQuotes!!!

```
#!haskell
>let e = [rame|
    R1 <- 2
    R2 <- 4
    R0 <- R1 + R2|]
> compute $ loadProgram e
```

[1]: https://www.cs.toronto.edu/~sacook/homepage/rams.pdf "Time Bounded Random Access Machines"
